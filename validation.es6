/* jshint esversion: 6, browser: true, debug: false, -W038, -W018 */

/**
 * @module
 */

const regexps = {
  phone: [/^[\d\s\-]+$/, /\d+/g],
  email: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
  whitespace: /[\s\uFEFF\xA0]/
};

/**
 * Safely parsing JSON
 * @param  {string} json - stringified JSON to parse
 * @return {object} json - parsed JSON (object literal)
 */
function parseJSON(json) {
  try {
    return JSON.parse(json);
  } catch(ex) {}
}

/**
* Execute a function when the DOM is fully loaded
* @param {function} callback - fired when DOM is loaded
*/
export const domReady = function() {
  const queue = [];
  document.addEventListener("DOMContentLoaded", function ready() {
    document.removeEventListener("DOMContentLoaded", ready);
    let callback;
    while ((callback = queue.shift())) callback();
  });
  return callback => {
    if (typeof callback !== "function")
      return;
    if (document.readyState === "complete")
      callback();
    else
      queue.push(callback);
  };
}();

/**
 * @class
 */
class Base {
  constructor() {
    this.listeners = {};
  }

  on(event, callback) {
    if (typeof callback === "function")
      (this.listeners[event] || (this.listeners[event] = [])).push(callback);
    return this;
  }

  off(event, callback) {
    if (this.listeners[event] && typeof callback === "function") {
      const index = this.listeners[event].indexOf(callback);
      if (index !== -1)
        this.listeners[event].splice(index, 1);
    }
    return this;
  }

  valid(...args) {
    return this.then(...args);
  }

  invalid() {
    return this.catch(...args);
  }

  then(callback) {
    return this.on("valid", callback);
  }

  catch(callback) {
    return this.on("invalid", callback);
  }

  get isValid() {
    if (!this.validate)
      throw new ReferenceError(`This class doesn't implement a needed "validate()" method`);
    return !!this.validate();
  }

  get isInvalid() {
    return !this.isValid;
  }
}

export class Validation extends Base {
  constructor(input, type, setup = {}) {
    super();
    this.input = input;
    this.type = (type || input.dataset.validationType).toLowerCase();
    this.setup = setup || parseJSON(input.dataset.validationSetup);
    if (!this.setup)
      throw new TypeError("No valid setup was provided");
    this.message = ({ message } = this.setup).message;

    input.addEventListener("change", () => {
      const isValid = this.validate(input, type);
      ((isValid ? this.listeners.valid : this.listeners.invalid) || [])
        .forEach(callback => {
          callback(input, input.value);
        });
      if (isValid)
        this.message.classList.remove("invalid");
      else
        this.message.classList.add("invalid");
    }, false);

    if (message) {
      if (typeof message === "string")
        (this.message = input.parentElement.appendChild(document.createElement("div"))).classList.add("validation");
      else if (message.nodeType === 3)
        this.message = message;
    }
  }

  validate() {
    const { type, value, checked, validity: { valid } } = this.input;
    if (!this.type || !this.type.length)
      return type === "checkbox" || type === "radio" ? checked : !!value.length;
    if (!value.length)
      return false;
    const isNaN = window.isNaN(+value);
    switch (this.type) {
      case "email":
        return regexps.email.test(value);
      case "phone":
        return regexps.phone[0].test(value) && value.match(regexps.phone[1]).join("").length >= (this.setup[length] || 6) && this.setup[prefix] ? !value.indexOf(this.setup[prefix]) : true;
      case "cardnumber":
      case "creditcard":
        if (isNaN)
          return false;
        const sumTable = [
          [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
          [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]
        ];
        let sum = 0, flip = 0;

        for (let i = value.length - 1; i >= 0; i--)
          sum += sumTable[flip++ & 0x1][value[i]];

        return !(sum % 10);
      case "cvv":
        return !isNaN && value.length >= 3 && value.length <= 4;
      case "number":
        return !isNaN;
      case "signs":
        return !new RegExp(`[^${setup[signs]}]`).test(value);
      default:
        return typeof this.type === "function" ? this.type() : regexps[this.type] && regexps[this.type].test(value) || valid;
    }
  }
}

export class FormValidation extends Base {
  constructor(form, setup = {}) {
    super();
    this.form = form;
    this.inputs = [];
    this.inputs.valid = new Set();
    this.inputs.invalid = new Set();
    this.setup = setup;

    domReady(() => {
      if (!form && typeof form === "string")
        var form = this.form = document.querySelector(form) || document.querySelector("form");
      if (!form)
        return;
      [].push.apply(this.inputs, form.querySelectorAll(".required, input[required], select[required], textarea[required]"));
      this.inputs.forEach(this.add.bind(this));
      form.addEventListener("submit", () => {
        if (this.inputs.invalid.length) {
          e.preventDefault();
          this.trigger("invalid");
        }
      });
    });
  }

  add(input) {
    if (input instanceof Validation) {
      this.inputs.push(input.input);
      this.inputs[input.isValid ? "valid" : "invalid"].push(input.input);
    }
    (input instanceof Validation ? input : new Validation(input, this.setup[input.name]))
      .then(() => {
        this.inputs.valid.add(input);
        this.inputs.invalid.remove(input);
        this.trigger("valid", input);
      })
      .catch(() => {
        this.inputs.invalid.add(input);
        this.inputs.valid.remove(input);
        this.trigger("invalid", input);
      });
    return this;
  }

  remove(input) {
    if (this.inputs.indexOf(input) === -1)
      return this;
    this.inputs.splice(this.inputs.indexOf(input), 1);
    this.inputs.valid.remove(input);
    this.inputs.invalid.remove(input);
    return this;
  }

  get delete() {
    return this.remove;
  }

  trigger(type, input) {
    switch (type) {
      case "invalid":
        (this.listeners.invalid || []).forEach(callback => {
          callback(input || this.inputs.invalid);
        });
        break;
      case "valid":
      (this.listeners.valid || []).forEach(callback => {
        callback(input || this.inputs.valid);
      });
    }
  }

  get isValid() {
    return this.valid.length === this.inputs.length;
  }
}